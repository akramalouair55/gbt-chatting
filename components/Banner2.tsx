import { useEffect, useRef } from 'react'
export default function Banner2(): JSX.Element {
    const banner = useRef<HTMLDivElement>()

    const atOptions = {
        key: '06420d69f863f9337cdea36cd9ea3ebe',
        format: 'iframe',
		height : 50,
	width : 320,
        params: {},
    }
    useEffect(() => {
    //@ts-ignore
    if (!banner.current.firstChild) {
        const conf = document.createElement('script')
        const script = document.createElement('script')
        script.type = 'text/javascript'
        script.src = `//beckoverreactcasual.com/${atOptions.key}/invoke.js`
        conf.innerHTML = `atOptions = ${JSON.stringify(atOptions)}`

        if (banner.current) {
            banner.current.append(conf)
            banner.current.append(script)
        }
    }
}, [])
    //@ts-ignore
    return <div className="mx-2 my-5 border border-gray-200 justify-center items-center text-white text-center" ref={banner}></div>
}